package com.panacloud.az3ez.todolist;

import android.content.Context;
import android.graphics.Paint;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.TextView;

import java.util.ArrayList;

/**
 * Created by panacloud on 3/26/15.
 */
public class MainList extends Fragment {
    View view;
    Toolbar tb;
    ListView lv;
    Button b;
    EditText et;
    ArrayList<String> st;
    TextView item;
    String[] a;
//    String[] todo= {"Task1","Task2","Task3","Task2","Task3","Task2","Task3","Task2","Task3"};

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        st = new ArrayList<String>();

        if(savedInstanceState != null)
            st = savedInstanceState.getStringArrayList("this");
    }

    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.main_fragment,container,false);
        tb = (Toolbar) view.findViewById(R.id.toolbar);
        tb.setTitle("My To-Do List");
        tb.setSubtitle("Today's List");
        tb.setNavigationIcon(tb.getNavigationIcon());
        et = (EditText) view.findViewById(R.id.edit);
        b = (Button) view.findViewById(R.id.add_btn);
        lv = (ListView) view.findViewById(R.id.listView);
        b.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(!et.getText().toString().equals(""))
                    st.add(et.getText().toString());
                a = st.toArray(new String[st.size()]);
                et.setText("");
                MyAdapter adapter = new MyAdapter(getActivity().getBaseContext(),android.R.layout.simple_list_item_1,
                        a);
                lv.setAdapter(adapter);
            }
        });
        return view;

    }

    public class MyAdapter extends ArrayAdapter<String>{
            View v;
        public MyAdapter(Context context, int resource, String[] objects) {
            super(context, resource, objects);
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent) {
            LayoutInflater li = (LayoutInflater) getActivity().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            v = li.inflate(R.layout.single_item,parent,false);
            final CheckBox checkBox1 = (CheckBox) v.findViewById(R.id.checkBox);
            item = (TextView) v.findViewById(R.id.it);
            item.setText(a[position%a.length]);
            checkBox1.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
                @Override
                public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                    if(isChecked){
                        Log.d("what","ww");
                        item.setPaintFlags(item.getPaintFlags()| Paint.STRIKE_THRU_TEXT_FLAG);
                    }else{
                        item.setPaintFlags(item.getPaintFlags()& (~Paint.STRIKE_THRU_TEXT_FLAG));
                    }
                }
            });
            return v;
        }
    }
public void work(){

}
    @Override
    public void onPause() {
        super.onPause();
        Bundle b = new Bundle();
        b.putStringArrayList("this",st);
    }
}
